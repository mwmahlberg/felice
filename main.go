/*
 *  main.go is part of felice, chat application based on Apache Kafka, written in Go
 *  Copyright (C) 2020 Markus Mahlberg
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package main

import (
	"log"

	"github.com/alecthomas/kong"
)

type message struct {
	User    string
	Message string
	Channel string
}

var cli struct {
	Kafka  string `help:"url of kafka server" env:"KAFKA" default:"localhost"`
	Server server `cmd:"" default:"1" help:"run server"`
	Client client `kong:"cmd,default='1',help='run client'"`
}

func init() {
	log.SetFlags(log.LstdFlags | log.Lmicroseconds)
}

func main() {
	ctx := kong.Parse(&cli, kong.Description("A chat server based on kafka"))
	ctx.FatalIfErrorf(ctx.Run())
}
