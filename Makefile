go := $(shell which go 2> /dev/null)
docker := $(shell which docker 2> /dev/null)
compose := $(shell which docker-compose 2> /dev/null)
image := "mwmahlberg/felice"
tag := "latest"
.PHONY: all up down restart .check-docker .check-go .check-compose

default: felice
.SILENT: felice docker

.check-docker:
ifndef docker
	$(error "docker not in $(PATH)")
endif

.check-go:
ifndef go
	$(error "go not in $(PATH)")
endif

.check-compose:
ifndef go
	$(error "docker-compose not in $(PATH)")
endif	

felice: *.go
	@echo Building felice binary
	@go build

docker: .check-docker Dockerfile
	@echo Building $(image):$(tag)
	@docker build -t $(image):$(tag) .

up: .check-compose
	@echo Starting environment
	@docker-compose up -d

down: .check-compose
	@echo Tearing down environment
	@docker-compose down --volumes

stop: .check-compose
	@echo Stopping environment
	@docker-compose stop

rebuild-compose:
	@echo Rebuilding environment
	$(MAKE) down
	$(MAKE) docker
	$(MAKE) up

all: felice rebuild-compose

clean:
	@echo Removing binary
	@$(RM) felice

rmi:
	@echo Removing image $(image):$(tag)
	@docker rmi $(image):$(tag)

really-clean:
	@echo Making $@
	@$(MAKE) down
	@$(MAKE) clean
	@$(MAKE) rmi

