FROM golang:alpine as build
WORKDIR /go/src/app
COPY . .
RUN apk update && apk add gcc librdkafka-dev openssl-libs-static zlib-static zstd-libs libsasl librdkafka-static lz4-static zstd-static musl-dev 
RUN go build

FROM alpine
ENV DOCKERIZE_VERSION v0.6.1
ENV KAFKA kafka
RUN apk --no-cache upgrade && apk --no-cache --virtual .get add curl \
 && curl -L -O https://github.com/jwilder/dockerize/releases/download/${DOCKERIZE_VERSION}/dockerize-linux-amd64-${DOCKERIZE_VERSION}.tar.gz \
 && tar -C /usr/local/bin -xzvf dockerize-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
 && rm dockerize-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
 && apk del .get \
 && apk add --no-cache librdkafka
COPY --from=build /go/src/app/felice /usr/local/bin/felice
CMD ["sh","-c","/usr/local/bin/dockerize -wait tcp://${KAFKA}:9092 /usr/local/bin/felice server"]