/*
 *  client.go is part of felice, chat application based on Apache Kafka, written in Go
 *  Copyright (C) 2020 Markus Mahlberg
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package main

import (
	"fmt"
	"log"
	"strings"
	"time"

	"github.com/gorilla/websocket"
	"github.com/marcusolsson/tui-go"
	"gopkg.in/confluentinc/confluent-kafka-go.v1/kafka"
)

type client struct {
	User     string `kong:"help='the username to connect with',env='LOGNAME'"`
	URL      string `kong:"help='server url',default='ws://localhost:8080'"`
	Room     string `kong:"help='room to connect to',default='felice'"`
	producer *kafka.Producer
}

type submitFunction func(*tui.Entry)

func (c *client) submit(conn *websocket.Conn, input *tui.Entry) submitFunction {
	return func(input *tui.Entry) {
		if err := conn.WriteJSON(&message{c.User, input.Text(), ""}); err != nil {
			panic(err)
		}
		input.SetText("")
	}
}

func (c *client) close(conn *websocket.Conn, ui tui.UI) func() {

	return func() {
		if err := conn.WriteControl(websocket.CloseMessage, nil, time.Now().Add(3*time.Second)); err != nil {
			log.Printf("closing connection: %s", err)
		}
		ui.Quit()
	}
}

func (c *client) gui(conn *websocket.Conn) tui.UI {
	history := tui.NewVBox()
	history.SetTitle("Chat")

	hScroll := tui.NewScrollArea(history)
	hScroll.SetAutoscrollToBottom(true)

	hbox := tui.NewVBox(hScroll)
	hbox.SetTitle("Chatbox")
	hbox.SetBorder(true)

	in := tui.NewEntry()
	in.SetFocused(true)
	in.SetSizePolicy(tui.Expanding, tui.Maximum)

	inputBox := tui.NewHBox(in)
	inputBox.SetBorder(true)
	inputBox.SetSizePolicy(tui.Expanding, tui.Maximum)

	chat := tui.NewVBox(hbox, inputBox)
	chat.SetSizePolicy(tui.Expanding, tui.Expanding)

	in.OnSubmit(c.submit(conn, in))

	ui, err := tui.New(chat)

	if err != nil {
		log.Fatal(err)
	}

	ui.SetKeybinding("Esc", c.close(conn, ui))
	ui.SetKeybinding("Up", func() { hScroll.Scroll(0, -1) })
	ui.SetKeybinding("Down", func() { hScroll.Scroll(0, 1) })
	ui.SetKeybinding("a", func() { hScroll.SetAutoscrollToBottom(true) })
	ui.SetKeybinding("t", func() { hScroll.ScrollToTop() })
	ui.SetKeybinding("b", func() { hScroll.ScrollToBottom() })

	ui.SetFocusChain(tui.DefaultFocusChain)
	go func() {
		var msg = &message{}
		var err error
		var m string
		for {
			err = conn.ReadJSON(msg)
			if err == nil {
				m = strings.TrimSuffix(string(msg.Message), "\n")
				history.Append(tui.NewHBox(
					tui.NewLabel(time.Now().Format("15:04")),
					tui.NewPadder(1, 0, tui.NewLabel(fmt.Sprintf("<%s>", msg.User))),
					tui.NewLabel(m), tui.NewSpacer(),
				))
				ui.Repaint()
			} else {
				// The client will automatically try to recover from all errors.
				log.Printf("Consumer error: %v (%v)\n", err, msg)
			}
		}
	}()

	return ui
}

func (c *client) Run() error {

	conn, r, err := websocket.DefaultDialer.Dial(c.URL+"/"+c.Room, nil)
	if err != nil {
		log.Printf("connecting to %s: %s", c.URL, err)
		return err
	} else if r.StatusCode >= 400 {
		log.Printf("Received response code %d: %s", r.StatusCode, r.Status)
	}

	// defer conn.Close()

	return c.gui(conn).Run()
}
