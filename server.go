/*
 *  server.go is part of felice, chat application based on Apache Kafka, written in Go
 *  Copyright (C) 2020 Markus Mahlberg
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package main

import (
	"bytes"
	"fmt"
	"io"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/gorilla/websocket"
	"github.com/twinj/uuid"
	"gopkg.in/confluentinc/confluent-kafka-go.v1/kafka"
)

type server struct {
	Port int `help:"port to listen to" env:"PORT" default:"8080"`
	pub  chan (*message)
	sub  chan (*message)
}

func chatHandler() http.Handler {

	var upgrader = websocket.Upgrader{
		ReadBufferSize:  1024,
		WriteBufferSize: 1024,
	}

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)
		room := vars["room"]
		var consumer *kafka.Consumer
		var producer *kafka.Producer
		in := make(chan []byte)
		defer close(in)

		conn, err := upgrader.Upgrade(w, r, nil)
		if err != nil {
			http.Error(w, "Upgrade required", 426)
			return
		}
		log.Printf("Handler started for %s, remote %s", room, r.RemoteAddr)
		defer conn.Close()

		// Forward messages from Kafka to client
		go func() {
			consumer, _ = kafka.NewConsumer(&kafka.ConfigMap{
				"bootstrap.servers": cli.Kafka,
				"group.id":          uuid.NewV4().String(),
				"auto.offset.reset": "earliest",
			})

			log.Printf("Error for subscription: %s", consumer.Subscribe(room, nil))

			for {
				msg, _ := consumer.ReadMessage(-1)
				log.Printf("Received message %s for %s", string(msg.Value), r.RemoteAddr)
				conn.WriteMessage(websocket.TextMessage, msg.Value)
			}
		}()

		go func() {
			// Write to kafka
			producer, _ = kafka.NewProducer(&kafka.ConfigMap{"bootstrap.servers": cli.Kafka})
			for b := range in {
				log.Printf("Received message from %s for %s: %s", r.RemoteAddr, room, string(b))
				producer.Produce(&kafka.Message{
					TopicPartition: kafka.TopicPartition{Topic: &room, Partition: kafka.PartitionAny},
					Value:          b,
				}, nil)
			}
		}()

		// Read from socket

		buf := bytes.NewBuffer(nil)
		for {
			t, rd, err := conn.NextReader()
			if err != nil {
				log.Printf("reading from websocket: %s", err)
				consumer.Close()
				producer.Close()
				return
			}
			switch t {
			case websocket.TextMessage:
				fallthrough
			case websocket.BinaryMessage:
				log.Printf("Received messages from %s", r.RemoteAddr)
				io.Copy(buf, rd)
				in <- buf.Bytes()
				buf.Reset()
			case websocket.CloseMessage:
				consumer.Close()
				producer.Close()
				return
			}
		}
	})
}

func (s *server) Run() error {
	log.Printf("Listening on port %d", s.Port)
	r := mux.NewRouter()
	r.Handle("/{room}", chatHandler())
	http.ListenAndServe(fmt.Sprintf(":%d", cli.Server.Port), r)
	return nil
}
