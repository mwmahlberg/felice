module bitbucket.org/mwmahlberg/felice

go 1.14

require (
	github.com/alecthomas/kong v0.2.4
	github.com/gorilla/mux v1.7.4
	github.com/gorilla/websocket v1.4.1
	github.com/marcusolsson/tui-go v0.4.0
	github.com/mattn/go-runewidth v0.0.8 // indirect
	github.com/myesui/uuid v1.0.0 // indirect
	github.com/twinj/uuid v1.0.0
	golang.org/x/text v0.3.2 // indirect
	gopkg.in/confluentinc/confluent-kafka-go.v1 v1.1.0
	gopkg.in/stretchr/testify.v1 v1.2.2 // indirect
)
