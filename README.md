Project
=======

[![License: AGPL v3](https://img.shields.io/badge/License-AGPL%20v3-blue.svg)](https://www.gnu.org/licenses/agpl-3.0) [![Bitbucket open issues](https://img.shields.io/bitbucket/issues/mwmahlberg/felice)][felice:issues] [![Docker Image Version (latest semver)](https://img.shields.io/docker/v/mwmahlberg/felice?label=docker%20image&sort=semver)]() [![Go Report Card](https://goreportcard.com/badge/bitbucket.org/mwmahlberg/felice)](https://goreportcard.com/report/bitbucket.org/mwmahlberg/felice)

A proof of concept for an [Apache Kafka][kafka] based chat application written in [Go][golang].

Project Status
--------------

This project is a private proof of concept. Use it all you like, but do not expect any support as for the time being.

If I am happy with the outcome, I might well turn it into a library ready to use.

Build
-----

This project uses [GNU make][gnu:make]. However, this is more of a convenience.
You can build felice perfectly without make. Just have a look at the Makefile on how to do stuff.

### Targets

| Target          | Description                                                    |
| --------------- | -------------------------------------------------------------- |
| felice          | Compiles the binary (default)                                  |
| docker          | Builds the docker container                                    |
| up              | Starts the environment defined in `docker-compose.yml`         |
| down            | Tears down the compose environment **and removes its volumes** |
| stop            | Stops the compose environment. Restart with `up`               |
| rebuild-compose | Alias for `down docker up`                                     |
| all             | Alias for `felice rebuild-compose`                             |
| `clean`         | Removes the binary                                             |
| `rmi`           | Removes the docker image                                       |
| `really-clean`  | Alias for `down clean rmi`                                     |

You can call the various targets with `make [target[ target]...]`, for example

```shell
$ make clean felice
Removing binary
Building felice binary
```

Usage
-----

### Preparations

You need to clone the repository and change into it

```shell
$ git clone https://mwmahlberg@bitbucket.org/mwmahlberg/felice.git
Cloning into 'felice'...
remote: Counting objects: 19, done.
remote: Compressing objects: 100% (18/18), done.
remote: Total 19 (delta 7), reused 0 (delta 0)
Unpacking objects: 100% (19/19), 20.54 KiB | 179.00 KiB/s, done.
$ cd felice
```

Now you can either set up the environment using a locally built image or you can use the [prebuilt image available on hub.docker.com][felice:image].

#### With a local build

You can build the environment and the client in one step

```shell
$ make felice up
Building felice binary
Starting environment
Creating network "felice_default" with the default driver
Creating volume "felice_zookeeper_data" with default driver
Creating volume "felice_kafka_data" with default driver
Building server
Step 1/11 : FROM golang:alpine as build
[...]
Creating felice_kafka_1     ... done
```

#### Using the image from hub.docker.com

First, you need to pull the docker image

```shell
$ docker pull mwmahlberg/felice
Using default tag: latest
latest: Pulling from mwmahlberg/felice
c9b1b535fdd9: Already exists
da703b02c485: Already exists
add6a9b8009e: Pull complete
Digest: sha256:571a1e517743f94750db9d011f48b5e744a9a813304baaf0df26f14c73601d34
Status: Downloaded newer image for mwmahlberg/felice:latest
docker.io/mwmahlberg/felice:latest
```

Then, you will need to build the client and start the environment

```shell
$ make felice up
Building felice binary
Starting environment
Creating network "felice_default" with the default driver
Creating volume "felice_zookeeper_data" with default driver
Creating volume "felice_kafka_data" with default driver
Creating felice_zookeeper_1 ... done
Creating felice_server_1    ... done
Creating felice_kafka_1     ... done
```

### Use the client

You have two options: you can use the client from within the docker image or
use the local client.

In order to see all options, you can call the binary with the `--help` flag:

```shell
Usage: felice <command>

A chat server based on kafka

Flags:
  --help                 Show context-sensitive help.
  --kafka="localhost"    url of kafka server ($KAFKA)

Commands:
  server
    run server

  client
    run client

Run "felice <command> --help" for more information on a command.
```

#### Using the local client

Simply call `./felice client --user=yourName`

#### Using the client from within the docker image

Call `docker exec -it felice_server_1 /usr/local/bin/felice --user=yourName`.

Support
-------

As written before, there is currently no support available. You may file issues, but it is unlikely that can put real effort into this project. It is just for fun.

However, there is an [issue tracker][felice:issues] set up.

License
-------

> Copyright (C) 2020 Markus Mahlberg
>
> This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.
>
> This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.
>
> You should have received a [copy of the GNU Affero General Public License][agplv3]
along with this program.  If not, see <https://www.gnu.org/licenses/>.

[kafka]: https://kafka.apache.org "Apache Kafka Project website"
[golang]: https://golang.org "Go project website"
[felice:issues]: https://bitbucket.org/mwmahlberg/felice/issues "Issue tracker on felice's project website"
[agplv3]: https://www.gnu.org/licenses/agpl-3.0.txt "GNU AFFERO GENERAL PUBLIC LICENSE, Version 3"
[gnu:make]: https://www.gnu.org/software/make/ "GNU make project website"
[felice:image]: https://hub.docker.com/repository/docker/mwmahlberg/felice "Image page on hub.docker.com"
